import Image from "next/image";
import React, { useState } from "react";
import { ErrorMessage, Field, Form, Formik, setNestedObjectValues } from "formik";
import { Checkbox, TextField, ToggleButtonGroup } from "formik-mui";
import { Button, ToggleButton } from "@mui/material";
import * as Yup from 'yup';
import Error from "next/error";
import { BACKEND_API } from "@/const";
import { useSession } from "next-auth/react";
import { useRouter } from "next/router";

export const Registration = () => {
  const router = useRouter();
  const {data: session} = useSession()

  const [page, setPage] = useState(0)
  const [checked, setChecked] = useState(false)
  const [errorChecked, setErrorChecked] = useState("")

  console.log(page)

  return (
    <div className="h-screen w-screen bg-gray">
      <div className="h-1/6 w-screen bg-primary pt-20 px-4">
        <div className="text-2xl font-bold">
          Dana Darurat
        </div>
      </div>
      <div className="px-6 mt-6">
        <Formik
          initialValues={{
            maritalStatus: null,
            savingMonths: null,
            expensePerMonth: null,
            emergencyFundTarget: null,
            paymentType: null
          }}
          validationSchema={Yup.object({
            expensePerMonth: Yup.number()
              .min(0, 'Pengeluaran bulanan tidak boleh lebih kecil dari 0')
              .required('Pengeluaran bulanan harus diisi'),
            maritalStatus: Yup.number()
              .required('Status pernikahan harus diisi'),
            savingMonths: Yup.number()
              .required('Opsi menabung per bulan harus diisi'),
            emergencyFundTarget: Yup.number()
              .min(0, 'Target dana darurat tidak boleh lebih kecil dari 0')
              .required('Target dana darurat harus diisi'),
            paymentType: Yup.number()
              .required('Opsi menabung harus diisi')
          })}
          onSubmit={async (values, {setSubmitting}) => {
            if (!checked) {
              setErrorChecked("Anda harus menyetujui")
              setSubmitting(false)
              return
            }

            const res = await fetch(`${BACKEND_API}/emergency_fund/`, {
              method: 'POST',
              headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${session?.user?.accessToken}`
              },
              body: JSON.stringify({
                values
              })
            })

            const data = await res.json()
            console.log(data)

            router.push('/')

          }}
        >
          {
            ({isSubmitting, setTouched, values, validateForm, errors, touched}) => (
              <Form>
                <div className={`${page !== 0 ? "hidden" : 'visible'}`}>
                  <Field className="my-1" fullWidth component={TextField} name="expensePerMonth"
                         label="Pengeluaran Bulanan" type="number"
                         variant="standard"/>
                  <div className="text-md my-2 text-gray-text">Status Pernikahan</div>
                  <Field className="my-1 " component={ToggleButtonGroup} exclusive fullWidth name="maritalStatus"
                         type="checkbox" disabled={isSubmitting}>
                    <ToggleButton value={0} style={{textTransform: 'none', fontSize: '0.75rem'}}>Lajang</ToggleButton>
                    <ToggleButton value={1} style={{textTransform: 'none', fontSize: '0.75rem'}}>Menikah</ToggleButton>
                    <ToggleButton value={2} style={{textTransform: 'none', fontSize: '0.75rem'}}>Menikah +
                      Anak</ToggleButton>
                  </Field>
                  {errors.maritalStatus && touched.maritalStatus &&
                    <div className="text-error text-xs">{errors.maritalStatus}</div>}
                  <div className="text-md my-2 text-gray-text">Opsi Menabung per bulan</div>
                  <Field className="my-1 " component={ToggleButtonGroup} exclusive fullWidth name="savingMonths"
                         type="checkbox" disabled={isSubmitting}>
                    <ToggleButton value={0} style={{textTransform: 'none', fontSize: '0.75rem'}}>3</ToggleButton>
                    <ToggleButton value={1} style={{textTransform: 'none', fontSize: '0.75rem'}}>6</ToggleButton>
                    <ToggleButton value={2} style={{textTransform: 'none', fontSize: '0.75rem'}}>9</ToggleButton>
                    <ToggleButton value={3} style={{textTransform: 'none', fontSize: '0.75rem'}}>12</ToggleButton>
                  </Field>
                  {errors.savingMonths && touched.savingMonths &&
                    <div className="text-error text-xs">{errors.savingMonths}</div>}

                  <Field className="my-1" fullWidth component={TextField} name="emergencyFundTarget"
                         label="Target Dana Darurat" type="number" value={values.emergencyFundTarget}
                         variant="standard"/>

                  <div className="w-full">
                    <div
                      onClick={() => {
                        validateForm().then((data) => {
                          delete data.paymentType

                          console.log('masuk sini', values, errors, touched)
                          if (Object.keys(data).length > 0) {
                            setTouched(setNestedObjectValues(data, true));
                            return;
                          }
                          if (JSON.stringify(data) == '{}') setPage(1)
                        })
                      }}
                      className="py-4 rounded-full w-full mt-6 bg-primary text-black text-sm">
                      <div className="flex justify-center items-center">Tabung Sekarang</div>
                    </div>
                  </div>
                </div>


                <div className={`${page !== 1 ? "hidden" : 'visible'}`}>
                  <div className="text-xl font-extrabold">
                    Konfirmasi Pembukaan Rekening
                  </div>
                  <Field className="my-1" fullWidth component={TextField} name="emergencyFundTarget"
                         label="Target Dana Darurat" type="number"
                         value={values.emergencyFundTarget}
                         variant="standard"/>
                  <div className="my-2">Opsi Menabung</div>
                  <Field className="my-1 " component={ToggleButtonGroup} exclusive fullWidth name="paymentType"
                         type="checkbox" disabled={isSubmitting}>
                    <ToggleButton value={0} style={{textTransform: 'none', fontSize: '0.75rem'}}>Tabung
                      Manual</ToggleButton>
                    <ToggleButton value={1}
                                  style={{textTransform: 'none', fontSize: '0.75rem'}}>Autodebit</ToggleButton>
                    <ToggleButton value={2} style={{textTransform: 'none', fontSize: '0.75rem'}}>Bayar
                      Sekaligus</ToggleButton>
                  </Field>
                  {errors.paymentType && touched.paymentType &&
                    <div className="text-error text-xs">{errors.paymentType}</div>}

                  <div className="flex my-2">
                    <input className="accent-gray" type="checkbox" onChange={(e) => {
                      setChecked((prevState) => !prevState)
                    }}/>
                    <div className="px-2 text-sm">
                      Dengan menyetujui ini Anda akan membuat rekening dana darurat baru di SashimiBank
                    </div>
                  </div>

                  <button className="w-full">
                    <div
                      onSubmit={(e) => {
                        e.preventDefault()
                      }}
                      className="py-4 rounded-full w-full bg-primary text-black text-sm">
                      <div className="flex justify-center items-center">Tabung Sekarang</div>
                    </div>
                  </button>
                </div>
              </Form>
            )
          }

        </Formik>
      </div>
    </div>
  )
}

export default Registration