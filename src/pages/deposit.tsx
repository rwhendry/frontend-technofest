import Image from "next/image";
import React, { useEffect, useState } from "react";
import { useRouter } from "next/router";
import { useSession } from "next-auth/react";
import { BACKEND_API } from "@/const";
import { TextField } from "@mui/material";

export const Deposit = () => {
  const router = useRouter()
  const {data: session} = useSession()
  const [danaDarurat, setDanaDarurat] = useState(null)
  const [value, setValue] = useState(0)

  let percentage = 0
  let sisa = 0
  if (danaDarurat) {
    percentage = (danaDarurat['balance'] / danaDarurat['emergency_fund_target']).toFixed(2) * 100
    sisa = danaDarurat['emergency_fund_target'] - danaDarurat['balance']
  }

  useEffect(() => {
    const fetchAccountInfo = async () => {
      console.log('masuk sini')
      const res = await fetch(`${BACKEND_API}/emergency_fund/`, {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${session?.user?.accessToken}`,
        }
      })

      return await res.json()
    }

    const tmp = fetchAccountInfo().then((data) => {
      setDanaDarurat(JSON.parse(data))
      return data
    })
  }, [session])
  if (!danaDarurat) return (<div/>)

  return (
    <div className="h-screen w-full bg-gray">
      <div className="h-1/6 w-screen bg-primary pt-20 px-4 pb-16">
        <div className="text-2xl font-bold">
          Dana Darurat
        </div>
      </div>
      <div className="relative bg-slate-300 w-screen -top-4 px-4">
        <div className="p-4 bg-white rounded-md">
          <div className="flex divide-x">
            <Image className="mx-2" src='./savings.svg' alt={'test'} width={32} height={32}/>
            <div className="px-2">
              <div className="text-sm">Dana darurat terkumpul</div>
              <div className="font-bold">Rp. {danaDarurat['balance'] ?? 0}</div>
            </div>

            {/*<div className="px-2 text-sm">Kamu belum memiliki dana darurat, ayo buat tabungan dana daruratmu sekarang!*/}
            {/*</div>*/}
          </div>
        </div>
      </div>
      <div className="px-4 my-2">
        <div className="w-full bg-white rounded-full">
          <div
            className="bg-green w-full text-xs font-medium text-blue-100 text-center rounded-full py-4 whitespace-nowrap"
            style={{'width': `${percentage}%`}}>
            <div className="ml-2 font-light">Rp. {sisa} lagi hingga target dana daruratmu tercapai!</div>
          </div>
        </div>
      </div>

      <div className="flex mt-20 justify-center">
        <div className="font-bold"> Jumlah penambahan tabungan</div>
      </div>
      <div className="flex justify-center my-2">
        <TextField className="bg-white" type="number" onChange={(e) => {
          setValue(+e.target.value)
        }}/>
      </div>
      <div className="flex justify-center">
        <button className="py-2 mt-2 rounded-full bg-primary text-black w-full mx-6" type="submit" onClick={() => {
          const fetchData = async () => {
            const res = await fetch(`${BACKEND_API}/tabung/`, {
              method: 'POST',
              headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${session?.user?.accessToken}`,
              },
              body: JSON.stringify({
                balance: value
              })
            })
            const tmp = await res.json()

            return tmp
          }
          fetchData().then(() =>
            setValue(0)
          )
          setValue(0)
          router.push('/')
        }}>Tabung Sekarang
        </button>
      </div>
    </div>
  )
}

export default Deposit