// next-auth.js
import NextAuth, { SessionStrategy } from "next-auth";
import CredentialsProvider from "next-auth/providers/credentials";
import { BACKEND_API, HACKATHON_API } from "@/const";
import Credentials from "next-auth/providers/credentials";

const options = {
  providers: [
    CredentialsProvider({
      name: "Login",
      credentials: {
        username: {
          label: "username",
          type: "text",
        },
        password: {
          label: "Password",
          type: "password",
        }
      },
      async authorize(credentials, req) {
        console.log(credentials)
        const res = await fetch(`${BACKEND_API}/login/`, {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
            username: credentials?.username,
            loginPassword: credentials?.password
          }),
        })

        const user = await res.json()

        console.log(user, 'user')
        if (res.ok && user.success) {
          return user.data
        }

        throw new Error(JSON.stringify(user.errMsg))
      }
    })
  ],
  secret: "sEpi42D7Htbje0aDM8bePqQ/gp63IS+Ahik305aDU+4",
  jwt: {
    maxAge: 2 * 60,
  },
  pages: {
    signIn: '/login'
  },
  callbacks: {
    async jwt({token, user}) {
      return {...token, ...user};
    },
    async session({session, token, user}) {
      session.user = token as any;
      return session;
    },
  },
};

export default NextAuth(options);
