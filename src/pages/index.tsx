"use client";
import { useSession } from "next-auth/react";
import { Box, Button, Modal, Tab, Tabs } from "@mui/material";
import React, { useEffect, useState } from "react";
import { TabPanel } from "@/components/TabPanel";
import { Register } from "@/components/Register";
import { Login } from "@/components/Login";
import { BACKEND_API } from "@/const";
import Image from "next/image";
import { useRouter } from "next/router";

export const AuthPage = () => {
  const [value, setValue] = useState(0)
  const setLogin = () => {
    setValue(1)
  }
  return (
    <div className="p-12 bg-gray h-full min-h-screen">
      <div className="py-2 mt-16 text-2xl font-semibold">Selamat Datang di SashimiBank!</div>
      <div>Ayo buat akun sekarang</div>

      <Tabs variant="fullWidth" value={value} onChange={(e, val) => setValue(val)}>
        <Tab style={{textTransform: 'none'}} label="Daftar"/>
        <Tab style={{textTransform: 'none'}} label="Masuk"/>
      </Tabs>

      <TabPanel value={value} index={0}>
        <Register setLogin={setLogin}/>
      </TabPanel>
      <TabPanel value={value} index={1}>
        <Login/>
      </TabPanel>
    </div>
  )
}

export const MainPage = () => {
  const router = useRouter()
  const {data: session} = useSession()
  const [danaDarurat, setDanaDarurat] = useState(null)
  const [open, setOpen] = useState(false)

  let percentage = 0
  let sisa = 0
  if (danaDarurat) {
    console.log(danaDarurat, typeof danaDarurat)
    percentage = (danaDarurat['balance'] / danaDarurat['emergency_fund_target']).toFixed(2) * 100
    console.log(percentage, 'percentage', danaDarurat.balance)
    sisa = danaDarurat['emergency_fund_target'] - danaDarurat['balance']
  }

  useEffect(() => {
    const fetchAccountInfo = async () => {
      console.log('masuk sini')
      const res = await fetch(`${BACKEND_API}/emergency_fund/`, {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${session?.user?.accessToken}`,
        }
      })

      return await res.json()
    }

    const tmp = fetchAccountInfo().then((data) => {
      console.log(data)
      setDanaDarurat(JSON.parse(data))
      return data
    })
  }, [session])

  const style = {
    position: 'absolute' as 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 400,
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4,
  };


  if (danaDarurat) return (
    <div className="h-full w-screen bg-gray pb-4">
      <div className="h-1/6 w-screen bg-primary pt-20 px-4 pb-16">
        <div className="text-2xl font-bold">
          Dana Darurat
        </div>
      </div>
      <div className="relative bg-slate-300 w-screen -top-4 px-4">
        <div className="p-4 bg-white rounded-md">
          <div className="flex divide-x">
            <Image className="mx-2" src='./savings.svg' alt={'test'} width={32} height={32}/>
            <div className="px-2">
              <div className="text-sm">Dana darurat terkumpul</div>
              <div className="font-bold">Rp. {danaDarurat['balance'] ?? 0}</div>
            </div>

            {/*<div className="px-2 text-sm">Kamu belum memiliki dana darurat, ayo buat tabungan dana daruratmu sekarang!*/}
            {/*</div>*/}
          </div>
        </div>
      </div>
      <div className="px-4 my-2">
        <div className="w-full bg-white rounded-full">
          <div
            className="bg-green w-full text-xs font-medium text-blue-100 text-center rounded-full py-4 whitespace-nowrap"
            style={{'width': `${percentage}%`}}>
            <div className="ml-2 font-light">Rp. {sisa} lagi hingga target dana daruratmu tercapai!</div>
          </div>
        </div>
      </div>

      <div className="w-screen flex justify-center">
        <button
          onClick={() => setOpen(true)}
          className="py-4  rounded-full bg-primary text-black w-3/4 mx-2 justify-center items-center flex text-sm"> Cairkan
          Dana
        </button>
        <Modal
          open={open}
          onClose={() => setOpen(false)}
          aria-labelledby="modal-modal-title"
          aria-describedby="modal-modal-description"
        >
         <Box sx={style} className="w-full h-2/3">
           <div className="font-bold text-lg">
             Apakah kamu yakin ingin menarik dana daruratmu?
           </div>
           <div className="text-sm font-light">
             Sebagai pengingat, ketika ingin menggunakan dana darurat tanyakan pertanyaan kunci di bawah ini kepada diri sendiri:
             1. Apakah hal ini bersifat darurat, mendesak, dan tidak diperkirakan sebelumnya?
             2. Apakah hal ini merupakan suatu kebutuhan?
             3. Apakah hal ini harus segera dibayarkan?
             4. Apakah hal ini sangat penting dan berpotensi mengganggu kehidupan Anda? Jika semua jawaban adalah “Ya”, maka dana darurat dapat digunakan.
           </div>
           <div className="w-full mt-4 flex justify-center">
             <button
               onClick={() => {
                 const fetchData = async () => {
                   const res = await fetch(`${BACKEND_API}/ambil/`, {
                     method: 'POST',
                     headers: {
                       'Content-Type': 'application/json',
                       'Authorization': `Bearer ${session?.user?.accessToken}`,
                     },
                   })
                   const tmp = await res.json()

                   return tmp
                 }
                 fetchData().then(() => {})
                 setOpen(false)
                 router.push('/')
               }}
               className="py-4 px-8 rounded-full bg-primary text-black w-half px-2 justify-center items-center flex text-sm">Ya, Cairkan Dana
             </button>
           </div>

         </Box>
        </Modal>
        <button onClick={() => {
          router.push('/deposit')
        }}
          className="py-4 rounded-full bg-primary text-black w-3/4 mx-2 justify-center items-center flex text-sm"> Tabung
          Lagi
        </button>
      </div>
      <div className="w-screen flex justify-center mt-6 font-bold text-xl">
        Apa Itu Dana Darurat?
      </div>
      <div className="w-screen justify-center mt-6 text-xs px-10 text-justify">
        Dana darurat adalah sejumlah uang yang penting untuk disiapkan agar dapat mengantisipasi situasi darurat seperti
        sakit, kecelakaan, bencana alam, badai PHK, dan kejadian darurat lainnya. Keberadaan dana darurat ini wajib
        dimiliki setiap orang agar apabila sewaktu-waktu terjadi hal yang tidak diinginkan, kebutuhan hidupmu dan
        keluarga tetap bisa terpenuhi tanpa harus berutang. Tabungan dana darurat sebaiknya jadi sumber uang yang mudah
        diakses dan bisa kamu gunakan kapan saja, saat kejadian tak terduga yang bersifat darurat terjadi. Dengan
        membuka rekening dana darurat di SashimiBank, selain kamu dapat menabung dana daruratmu dengan mudah, kamu juga
        akan mendapat keuntungan sebesar 5% dari total dana darurat yang sudah kamu miliki setiap bulannya. Tunggu apa
        lagi? Ayo buat rekening dana daruratmu sekarang!
      </div>
      <div className="w-screen flex justify-center mt-4">
        <div className="w-screen mx-6 p-4 bg-primary flex">
          <Image src='./babi.svg' alt='icon babi' width={48} height={48}/>
          <div className="px-2">
            <div className="text-sm font-extrabold">
              Belum tahu kebutuhan Dana Daruratmu?
            </div>
            <div className="text-xs my-1">
              Hitung target dana daruratmu sekarang!
            </div>
          </div>
        </div>
      </div>
    </div>
  )

  return (
    <div className="h-full w-screen bg-gray pb-4">
      <div className="h-1/6 w-screen bg-primary pt-20 px-4 pb-16">
        <div className="text-2xl font-bold">
          Dana Darurat
        </div>
      </div>
      <div className="relative bg-slate-300 w-screen -top-4 px-4">
        <div className="p-4 bg-white rounded-md">
          <div className="flex divide-x">
            <Image className="mx-2" src='./savings.svg' alt={'test'} width={32} height={32}/>
            <div className="px-2 text-sm">Kamu belum memiliki dana darurat, ayo buat tabungan dana daruratmu sekarang!
            </div>
          </div>
        </div>
      </div>
      <div className="w-screen flex justify-center">
        <button
          onSubmit={() => {
            console.log('wtf')
            router.push('/registration')
          }}
          className="py-4 px-8 rounded-full bg-primary text-black w-half px-2 justify-center items-center flex text-sm"> Tabung
          Sekarang
        </button>
      </div>
      <div className="w-screen flex justify-center mt-6 font-bold text-xl">
        Apa Itu Dana Darurat?
      </div>
      <div className="w-screen justify-center mt-6 text-xs px-10 text-justify">
        Dana darurat adalah sejumlah uang yang penting untuk disiapkan agar dapat mengantisipasi situasi darurat seperti
        sakit, kecelakaan, bencana alam, badai PHK, dan kejadian darurat lainnya. Keberadaan dana darurat ini wajib
        dimiliki setiap orang agar apabila sewaktu-waktu terjadi hal yang tidak diinginkan, kebutuhan hidupmu dan
        keluarga tetap bisa terpenuhi tanpa harus berutang. Tabungan dana darurat sebaiknya jadi sumber uang yang mudah
        diakses dan bisa kamu gunakan kapan saja, saat kejadian tak terduga yang bersifat darurat terjadi. Dengan
        membuka rekening dana darurat di SashimiBank, selain kamu dapat menabung dana daruratmu dengan mudah, kamu juga
        akan mendapat keuntungan sebesar 5% dari total dana darurat yang sudah kamu miliki setiap bulannya. Tunggu apa
        lagi? Ayo buat rekening dana daruratmu sekarang!
      </div>
      <div className="w-screen flex justify-center mt-4">
        <div className="w-screen mx-6 p-4 bg-primary flex">
          <Image src='./babi.svg' alt='icon babi' width={48} height={48}/>
          <div className="px-2">
            <div className="text-sm font-extrabold">
              Belum tahu kebutuhan Dana Daruratmu?
            </div>
            <div className="text-xs my-1">
              Hitung target dana daruratmu sekarang!
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default function Home() {
  const {data: session} = useSession()

  console.log(session)
  return (
    <>
      {!session ? <AuthPage/> : <MainPage/>}
    </>
  )
}
