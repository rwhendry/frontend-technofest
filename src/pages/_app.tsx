import '@/styles/globals.css'
import type { AppProps } from 'next/app'
import React from "react";
import Provider from "../../app/Provider";
import { LocalizationProvider } from "@mui/x-date-pickers";
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";

export default function App({Component, pageProps}: AppProps) {
  return (
    <LocalizationProvider dateAdapter={AdapterDayjs}>
      <Provider>
        <Component {...pageProps} />
      </Provider>
    </LocalizationProvider>
  )
}

