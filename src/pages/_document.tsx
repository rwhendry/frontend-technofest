import { Html, Head, Main, NextScript } from 'next/document'
import Provider from "../../app/Provider";

export default function Document() {
  return (
    <Html lang="en">
      <Head/>
      <body>
        <Main/>
        <NextScript/>
      </body>
    </Html>
  )
}
