import { Field, Form, Formik, } from "formik";
import { TextField, ToggleButtonGroup } from "formik-mui";
import { DatePicker as MuiDatePicker } from "@mui/x-date-pickers";
import { ToggleButton } from "@mui/material";
import { getCsrfToken } from "next-auth/react";
import * as Yup from 'yup';
import { BACKEND_API } from "@/const";
import moment from "moment/moment";
import { useState } from "react";

const DatePicker = ({field, form, error, blur, ...props}) => {
  // Handle date change and update Formik field value
  const handleChange = (newValue) => {
    form.setFieldValue(field.name, newValue);
  };

  return (
    <MuiDatePicker
      {...field}
      {...props}
      disableFuture
      value={field.value || null}
      label="Tanggal Lahir"
      format="DD-MM-YYYY"
      onChange={handleChange}
      helperText={"harus disii"}
      slotProps={{textField: {variant: 'standard', error: error, fullWidth: true, disabled: blur}}}
    />
  );
};


export const Register = ({setLogin}) => {
  const phoneRegExp = /^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/
  const [error, setError] = useState(null)

  return (
    <div className="p-2">
      <Formik validateOnChange
              initialValues={{
                username: '',
                email: '',
                loginPassword: '',
                phoneNumber: '',
                ktpId: '',
                date: "",
                gender: null,
                birthDate: null,
              }}
              validationSchema={Yup.object({
                username: Yup.string()
                  .min(4, 'Username setidaknya terdiri dari 4 karakter')
                  .max(30, 'Username maksimal terdiri dari 30 karakter')
                  .required('Username harus diisi'),
                email: Yup.string()
                  .max(30, 'Email mksimal terdiri dari 30 karakter')
                  .email('Format email salah')
                  .required('Email harus diisi'),
                loginPassword: Yup.string()
                  .min(1, 'Passsword minimal terdiri dari 1 karakter')
                  .max(30, 'Password maksimal terdiri dari 30 karakter')
                  .required('Password harus diisi'),
                phoneNumber: Yup.string()
                  .matches(phoneRegExp, 'Nomor HP tidak valid')
                  .min(9, 'Nomor HP minimal teridri dari 10 angka')
                  .max(13, 'Nomor HP maksimal terdiri dari 13 angka')
                  .required('Nomor HP harus diisi'),
                ktpId: Yup.string()
                  .min(15, 'Nomor KTP harus terdiri dari 16 angka')
                  .max(17, 'Nomor KTP harus terdiri dari 16 angka')
                  .required('Nomor KTP harus diisi'),
                date: Yup.date()
                  .required('Tanggal Lahir harus diisi'),
                gender: Yup.number()
                  .required('Jenis kelamin harus diisi')
              })}
              onSubmit={async (values, {setSubmitting}) => {
                values = {
                  ...values,
                  birthDate: moment(values.date).format('DMMYYYY')
                }
                delete values.date

                const res = await fetch(`${BACKEND_API}/register/`, {
                  method: 'POST',
                  headers: {
                    'Content-Type': 'application/json'
                  },
                  body: JSON.stringify({
                    values
                  })
                })

                const data = await res.json()

                console.log(data, 'res')
                if (data?.error) {
                  setError(data?.error)
                }

                setSubmitting(false);
                setLogin();
              }}>
        {
          ({submitForm, isSubmitting, errors, touched}) => (
            <Form>
              <Field className="my-1" fullWidth component={TextField} name="username" label="Username" type="text"
                     variant="standard"/>
              <Field className="my-1" fullWidth component={TextField} name="email" label="Email" type="email"
                     variant="standard"/>
              <Field className="my-1" fullWidth component={TextField} name="loginPassword" label="Password"
                     type="password"
                     variant="standard"/>
              <Field className="my-1" fullWidth component={TextField} name="phoneNumber" label="Nomor HP" type="number"
                     variant="standard"/>
              <Field className="my-1" fullWidth component={TextField} name="ktpId" label="Nomor KTP" type="number"
                     variant="standard"/>
              <Field
                className="my-1"
                component={DatePicker}
                name="date"
                error={errors.date && touched.date}
                blur={isSubmitting}
              />
              {errors.date && touched.date && <span className="text-error text-xs">Tanggal lahir harus diisi</span>}
              {!(errors.gender && touched.gender) ? <div className="my-1 text-sm">Jenis Kelamin</div> : <div className="my-1 text-sm text-error">Jenis Kelamin</div>}
              <Field className="my-1 " component={ToggleButtonGroup} exclusive fullWidth name="gender"
                     label="Jenis Kelamin" type="checkbox" disabled={isSubmitting}>
                <ToggleButton value={0} style={{textTransform: 'none'}}>Pria</ToggleButton>
                <ToggleButton value={1} style={{textTransform: 'none'}}>Wanita</ToggleButton>
              </Field>
              {errors.gender && touched.gender && <span className="text-error text-xs">Jenis kelamin harus diisi</span>}
              {error && <div className="text-error">{error}</div>}
              <button className="py-2 mt-2 rounded-full bg-primary text-black w-full px-2" type="submit">Daftar</button>
            </Form>
          )}
      </Formik>
    </div>)
}