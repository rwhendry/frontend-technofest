import { ReactNode } from "react";

interface TabPanelProps {
  children: ReactNode
  value: number,
  index: number
}

export const TabPanel = ({children, value, index}: TabPanelProps) => {
  return (
    <div>
      {value === index && (
        <div>
          {children}
        </div>
      )}
    </div>
  )
}