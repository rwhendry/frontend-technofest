import { Field, Form, Formik } from "formik";
import moment from "moment";
import { BACKEND_API } from "@/const";
import { TextField, ToggleButtonGroup } from "formik-mui";
import { Button, ToggleButton } from "@mui/material";
import { signIn } from "next-auth/react";

export const Login = () => {
  return (
    <div className="p-2">
      <Formik initialValues={{username: null, password: ''}}
              onSubmit={async (values, {setSubmitting}) => {
                console.log(values)

                const res = await signIn('credentials', {
                  redirect: false,
                  username: values.username,
                  password: values.password,
                });


                console.log(res)
                setSubmitting(false);
              }}>
        {
          ({submitForm, isSubmitting}) => (
            <Form>
              <Field className="my-1" fullWidth component={TextField} name="username" label="Username" type="text"
                     variant="standard"/>
              <Field className="my-1" fullWidth component={TextField} name="password" label="Password"
                     type="password"
                     variant="standard"/>
              <button className="py-2 mt-2 rounded-full bg-primary text-black w-full px-2" type="submit">Masuk</button>
            </Form>
          )}
      </Formik>
    </div>
  )
}