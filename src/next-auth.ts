// // next-auth.js
// import NextAuth from "next-auth";
// import CredentialsProvider from "next-auth/providers/credentials";
// import { HACKATHON_API } from "@/const";
//
// const options = {
//   providers: [
//     CredentialsProvider({
//       name: "Login",
//       credentials: {
//         username: {
//           label: "username",
//           type: "text",
//           placeholder: "sashimi-test",
//         },
//         password: {
//           label: "password",
//           type: "password",
//         }
//       },
//       async authorize(credentials, req) {
//         const res = await fetch(`${HACKATHON_API}/user/auth/token`, {
//           method: 'POST',
//           body: JSON.stringify(credentials),
//           headers: { 'Content-Type': 'application/json'}
//         })
//
//         const data = await res.json()
//         if (res.ok && data) {
//           return data
//         }
//
//         return null
//       }
//     })
//   ],
// };
//
// export default NextAuth(options);
